## paths
daily_timesteps = 59 # do not change it
Dataset_file_path = 'DatasetMerge.csv'
Model_file_path = 'Modelos/'
Figuras_file_path =  'Figuras/'


daily_timesteps = 59 # do not change it

## Creating Dataset
valid_set_size_percentage = 10 
steps = 2
seq_len = 59 # 59 one day

## Creating Model
units = [64,32]
dropout = 0.2

## Training Model
epoch = 5
batch_size = 32