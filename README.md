# PV Forecasting Model using LSTM

## Introduction

<p>This is a multivariate LSTM predictor to PV generation forecasting in two time frames: the next 15 minutes and the day ahead. Its input variables are the historical photovoltaic generation and historical data of climatic variables such as temperature, pressure, humidity, solar radiation (estimated) among others.</p>

<p>The proposed model contains two lstm blocks, followed by a dropout layer. The number of units for the LSTM blocks and the dropout rate can be modified on config.py file</p>

## Installation

<p>To install on Windows you must use the following commands within a terminal command prompt</p>

```
python -m venv venv
python -m pip install --upgrade pip
(windows - cmd) venv\Scripts\activate
pip install -r requirements.txt
```

## Input 

<p>The variables to be configured are as follows:</p>
<p>valid_set_size_percentage : percentage of data used for model validation</p>
<p>steps : number of steps ahead for forescasting</p>
<p>seq_len: number of time steps backward used as input to the model</p>
<p>units: Parameter for the LSTM model. Refers to the number of units for each LSTM block</p>
<p>dropout: Parameter for the LSTM model. Dropout rate</p>
<p>epoch:number of times that the model will be trained</p>
<p>batch_size: number of sequences trained for each iteration</p>


## Running Model

<p>To run LSTM predictor,set up the input variables mentioned in the previos section on the file config.py, then type in the terminal:<p>

```
python lstm_multi_step_gitlab.py
```

## Results

<p>The results obtained are:</p>
<p>1. Trained Model</p>
<p>2. 15 min forecast graph for validation data</p>
<p>3. Day-ahead forecast chart for a random day</p>